package com.todo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.todo.Dao.TodoDao;
import com.todo.entities.Todo;

@Controller
public class HomeController {
	@Autowired
	ServletContext context;
	
	@Autowired
	TodoDao todoDao;

	@RequestMapping("/home")
	public String home(Model m) {
		/*
		//Get using context listner
		List<Todo> list = (List<Todo>)context.getAttribute("list");
		*/
		
		List<Todo> list = this.todoDao.getAll();		
		String str = "Home";
		m.addAttribute("page", str);
		m.addAttribute("todos", list);
		return "home";
	}
	
	@RequestMapping("/add")
	public String addTodo(Model m) {
		
		Todo t = new Todo();
		m.addAttribute("page", "Add");
		m.addAttribute("todo", t);
		return "home";
	}
	
	@RequestMapping(value="/saveTodo", method=RequestMethod.POST)
	public String saveTodo(@ModelAttribute("todo") Todo t, Model m) {
		t.setTodoDate(new Date());
		/*
		//getting the todo list from context
		List<Todo> list = (List<Todo >)context.getAttribute("list");
		list.add(t);
		*/
		
		this.todoDao.save(t);
		
		m.addAttribute("message", "Successfully added...");
		System.out.println(t.toString());
		return "home";
	}
}
